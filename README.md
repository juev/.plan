[TOC]

# January 2019

## 2019-01-08
1. Moved from bash to zsh+antigen (https://github.com/juev/dotfiles/blob/master/zshrc). Simple and clean for me.
1. Moved from Pingdom to Uptime robot, because Pingdom not free anymore.

## 2019-01-09
Updated [go-create](https://github.com/juev/go-create) repository. Simplified and added go modules.

## 2019-01-10
Updated [go-create](https://github.com/juev/go-create) repository. Creating new go program structure.

## 2019-01-13
Creating repository [link-checker](https://github.com/juev/link-checker) on C. Simple program for getting web-page and checking all weblinks.

## 2019-01-15
Continue working on link-checker. Added link validation. Added Travis integration for building and deploy binary files to GitHub.

## 2019-01-22
Started learning on specialization from Yandex: C++ language.
First day I completed materials from fist week.

I have some issue with my code on link-checker. I don't understand how to work with dependencies, which not present on official repository. And I cannot build static binary with c++.

## 2019-01-23
Tried to solve "Task List" task from speicalisation, but checking system did not accept my decision. I tried to solve with several ways, but in each way I got a new error. Something wrong with me.

# Febrary 2019

## 2019-02-19
This month I tryed to switch back to Emacs. So sad, it not clearly and so hard for me.

# March 2019

## 2019-03-09
Yep, I lost a many time with Emacs. But in this time I cannot do it again.

Wrote several posts with links and post about netrw.

Worked on C++ project [link-checker](https://github.com/juev/link-checker). I rewrote all codebase. In this time I need to create handling for URL unions.

# April 2019

## 2019-04-01
My Emacs configuration is completed. And in this time I focus my mind on Haskell. Trying to rewrite link-checker on Haskell language. All books, what I read about Haskell is forgot. And I read them again.

## 2019-04-19
I have a lot of work, and I cannot get any project on my free time. Just work and rest on evening.

# May

## 2019-05-22
I forgot about my plans. And today I remembered this. Start working on [fortune-go](https://github.com/juev/fortune-go) project. I like Golang, because he so simple.

# June

## 2019-06-06

Work a lot on my emasculates configuration. After that I moved my tasks from Things to org-mode and beorg on my phone. 